// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract SocialNetwork{

    string public name;
    uint public postCount = 0;
    mapping(uint => Post) public posts;

    struct Post {
        uint id;
        string content;
        uint tipAmount;
        address payable author;
    }

    event PostCreated(
        uint id,
        string content,
        uint tipAmount,
        address payable author
    );

    event PostTripped(
        uint id,
        string content,
        uint tipAmount,
        address payable author
    );

    constructor() {
        name = "Dapp Social Network";
    }

    function createPost(string memory _content) public{

        require(bytes(_content).length > 0, "Content must not null");
        postCount++;
        
        posts[postCount] = Post(postCount, _content, 0, payable(msg.sender));

        //Trigger event
        emit PostCreated(postCount, _content, 0, payable(msg.sender));


    }

    function tipPost(uint _id) public payable {

        require(_id > 0 && _id <= postCount);

        //Fetch Post
        Post memory _post = posts[_id];
        //Fetch the author
        address payable _author = _post.author;
        //Pay the author
        payable(_author).transfer(msg.value);
        //Increment the tip amount
        _post.tipAmount = _post.tipAmount + msg.value;
        //Update posts
        posts[_id] = _post;
        //Trigger an event
        emit PostTripped(postCount, _post.content, _post.tipAmount, _author);



    }
}